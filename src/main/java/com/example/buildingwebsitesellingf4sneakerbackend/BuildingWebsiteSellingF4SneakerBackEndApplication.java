package com.example.buildingwebsitesellingf4sneakerbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildingWebsiteSellingF4SneakerBackEndApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildingWebsiteSellingF4SneakerBackEndApplication.class, args);
    }

}
